Name:		libsamplerate
Version:	0.2.2
Release:	1
Summary:	Sample Rate Converter for audio
License:	BSD-2-Clause license
URL:		https://github.com/libsndfile/libsamplerate
Source0:	https://github.com/libsndfile/%{name}/releases/download/%{version}/%{name}-%{version}.tar.xz
BuildRequires:	alsa-lib-devel gcc pkgconfig
BuildRequires:	fftw-devel >= 0.15.0 libsndfile-devel >= 1.0.6

%description
Secret Rabbit Code (aka libsamplerate) is a Sample Rate Converter for
audio. SRC is capable of arbitrary and time varying conversions; from
downsampling by a factor of 12 to upsampling by the same factor.  The
conversion ratio can also vary with time for speeding up and slowing
down effects.

%package 	devel
Summary:	The devel for %{name}
Requires:	%{name} = %{version}-%{release}, pkgconfig

%description devel
Include Files and Libraries mandatory for Development

%prep
%autosetup -n %{name}-%{version} -p1

%build
%configure --disable-dependency-tracking --disable-fftw
%{?disable_rpath}
make %{?_smp_mflags}

%install
%make_install
rm -rf $RPM_BUILD_ROOT%{_docdir}/libsamplerate0-dev _doc
cp -a docs _doc
rm _doc/CMakeLists.txt

%check
export LD_LIBRARY_PATH=`pwd`/src/.libs
make check
unset LD_LIBRARY_PATH

%post -p /sbin/ldconfig
%postun -p /sbin/ldconfig

%files
%doc AUTHORS COPYING README.md _doc/*
%{_libdir}/%{name}.so.*
%exclude %{_libdir}/%{name}.la

%files devel
%{_includedir}/samplerate.h
%{_libdir}/%{name}.so
%{_libdir}/pkgconfig/samplerate.pc

%changelog
* Sat Jan 28 2023 Qingqing Li <liqingqing3@huawei.com> - 0.2.2-1
- upgrade to 0.2.2

* Mon Dec 23 2019 wangshuo <wangshuo47@huawei.com> - 0.1.9-4
- Type:bugfix
- ID:NA
- SUG:NA
- DESC: modify make install.

* Fri Aug 23 2019 openEuler Buildteam <buildteam@openeuler.org> - 0.1.9-3
- Package init
